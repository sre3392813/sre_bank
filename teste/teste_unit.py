import os
import requests
import random

# Gera números aleatórios
randon_num = str(random.randint(10000, 99999))
rando_saldo = random.randint(10000, 99999)

# Obtém o valor de MY_VARIABLE da variável de ambiente
MY_VARIABLE = os.environ.get('MY_VARIABLE')
BASE_URL = MY_VARIABLE 

response = requests.get(BASE_URL)


def test_get_status():
    response = requests.get(f'{BASE_URL}/status')
    if response.status_code == 200:
        print("Teste test_get_status: PASSOU")
    else:
        print("Teste test_get_status: FALHOU")
        print("Status code:", response.status_code)

def test_get_extrato():
    response = requests.get(f'{BASE_URL}/extrato')
    if response.status_code == 200:
        print("Teste test_get_extrato: PASSOU")
    else:
        print("Teste test_get_extrato: FALHOU")
        print("Status code:", response.status_code)

def test_post_create():
    payload = {
        "documento": randon_num,
        "nome": "teste",
        "saldo": rando_saldo
    }
    response = requests.post(f'{BASE_URL}/create', json=payload)
    if response.status_code in [200, 201]:
        print("Teste test_post_create: PASSOU")
    else:
        print("Teste test_post_create: FALHOU")
        print("Status code:", response.status_code)

def test_post_debito():
    payload = {
        "documento": randon_num,
        "nome": "teste",
        "saldo": rando_saldo
    }
    response = requests.post(f'{BASE_URL}/debito', json=payload)
    if response.status_code in [200, 201]:
        print("Teste test_post_debito: PASSOU")
    else:
        print("Teste test_post_debito: FALHOU")
        print("Status code:", response.status_code)

# Executa os testes
test_get_status()
test_get_extrato()
test_post_create()
test_post_debito()
