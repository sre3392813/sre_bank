terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "http" {
    address  = "https://gitlab.com/api/v4/projects/56375140/terraform/state/default"
    unlock_address = "https://gitlab.com/api/v4/projects/56375140/terraform/state/default/lock"
    username = var.TF_PASSWORD
    password = var.TF_USERNAME
  }
}


provider "aws" {
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  region     = "sa-east-1"
  # profile    = "default"
  token      = var.AWS_TOKEN
}
